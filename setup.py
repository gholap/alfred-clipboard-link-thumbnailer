#!/usr/bin/python

import json
import os
import sqlite3

env_vars = {
  var: os.environ[var]
  for var in [
      'alfred_workflow_data',
      'alfred_workflow_uid',
      'alfred_preferences'
  ]
}

with open('env-vars.json', 'w') as f:
  json.dump(env_vars, f)

WF_DATA_DIR = env_vars['alfred_workflow_data']
if not os.path.exists(WF_DATA_DIR):
  os.mkdir(WF_DATA_DIR)

THUMBS_DIR = WF_DATA_DIR + '/thumbs'
if not os.path.exists(THUMBS_DIR):
  os.mkdir(THUMBS_DIR)

with open(THUMBS_DIR + '/style.css', 'w') as css:
  css.write('''
    img {
      width: 100%;
    }
    html {
      background-color: var(--window-color);
      color: var(--result-text-color);
    }
    body {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 100%;
    }
    .caption {
      font-family: -apple-system;
      padding: 20px;
      text-align: center;
    }'''
  )


DB_PATH = WF_DATA_DIR + '/urlpreviews.sqlite'
thumbsdb = sqlite3.connect(DB_PATH)
thumbsdb.execute('''
  CREATE TABLE IF NOT EXISTS linkthumbs (
    link TEXT PRIMARY KEY,
    title TEXT,
    error BOOLEAN NOT NULL CHECK (error IN (0,1)),
    error_text TEXT,
    thumbnail_filepath TEXT
  )'''
)
thumbsdb.commit()
