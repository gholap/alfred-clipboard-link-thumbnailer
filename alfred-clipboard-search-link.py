#!/usr/bin/python
# -*- coding: utf-8 -*-


import HTMLParser
import os
import sqlite3

from alfred import *
from hashlib import md5


def unescape(html_escaped_str):
  try:
    return HTMLParser.HTMLParser().unescape(
      html_escaped_str.decode('iso-8859-1')
    )
  except:
    return html_escaped_str


def search_db(search_str):
  thumbsdb = sqlite3.connect(DB_PATH)
  thumbsdb.row_factory = sqlite3.Row
  likified = '%' + search_str + '%'
  return thumbsdb.execute('''
    SELECT *
    FROM linkthumbs
    WHERE link LIKE ? OR title LIKE ?''',
    (likified, likified)
  ).fetchall()


if __name__ == '__main__':
  search_str = sys.argv[1]
  matches = search_db(search_str)
  items = []
  for m in matches:
    title = unescape(m['title'])
    if not title.strip():
      title = '...'
    items.append({
      'title': title,
      'subtitle': u'[↩: paste][⌘↩: open] ' + m['link'],
      'arg': m['link'],
      'uid': m['link'],
      'quicklookurl': m['thumbnail_filepath']
    })

  print(json.dumps({'items': items}, indent=2))
