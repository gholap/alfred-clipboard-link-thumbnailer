#!/usr/bin/python

import json
import os
import sys

HOME = os.path.expanduser('~')
ALFDB_DIR = HOME + '/Library/Application Support/Alfred/Databases'
ALFDB_PATH = ALFDB_DIR + '/clipboard.alfdb'

WF_DIR = sys.path[0]

with open(WF_DIR + '/env-vars.json') as f:
  env_vars = json.load(f)


SCREENSHOTTER = WF_DIR + '/UrlScreenshotter.app/Contents/MacOS/UrlScreenshotter'

WF_DATA_DIR = env_vars['alfred_workflow_data']
THUMBS_DIR = WF_DATA_DIR + '/thumbs'
DB_PATH = WF_DATA_DIR + '/urlpreviews.sqlite'
