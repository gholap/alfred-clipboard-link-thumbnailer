import json
import subprocess
import sys
import uuid

def __tmp_file_path():
  return '/tmp/%s' % uuid.uuid4()

def read(filepath):
  json_filepath = __tmp_file_path()
  subprocess.call([
    'plutil', '-convert', 'json',
    '-o', json_filepath,
    filepath
  ])
  with open(json_filepath) as jf:
    return json.load(jf)

def write(obj, filepath):
  json_filepath = __tmp_file_path()
  with open(json_filepath, 'w') as jf:
    json.dump(obj, jf)
  subprocess.call([
    'plutil', '-convert', 'xml1',
    '-o', filepath,
    json_filepath
  ])


if __name__ == '__main__':
  print(json.dumps(read(sys.argv[1]), indent=2))
