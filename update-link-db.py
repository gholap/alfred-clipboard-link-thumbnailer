#!/usr/bin/python

import json
import os
import sqlite3
import subprocess

from alfred import *
from hashlib import md5
# from tqdm import tqdm
from urllib import urlencode
from uuid import uuid4

def links_in_alfdb():
  alfdb = sqlite3.connect(ALFDB_PATH)
  rows = alfdb.execute('''
    SELECT item
    FROM clipboard
    WHERE item LIKE "http://%" OR item LIKE "https://%"'''
  ).fetchall()
  return [r[0] for r in rows]


def hash(txt):
  h = md5()
  h.update(txt.encode('utf-8'))
  return h.hexdigest()


USELESS_TO_RETRY_ERRORS = set([
  'Frame load interrupted'  # when it is a download link, not an HTML page
])


def is_useless_to_retry(error_text):
  return (error_text in USELESS_TO_RETRY_ERRORS
          or error_text.endswith('malformed.'))

def thumbed_links_in_thumbsdb():
  thumbsdb = sqlite3.connect(DB_PATH)
  thumbsdb.row_factory = sqlite3.Row
  rows = thumbsdb.execute('''
    SELECT link, error, error_text
    FROM linkthumbs'''
  ).fetchall()
  thumbed_links = []
  for row in rows:
    if row['error'] == 0:
      thumbed_links.append(row['link'])
    elif is_useless_to_retry(row['error_text']):
      # Even though the link hasn't been thumbed,
      # having these particular errors means that it will never
      # be, no matter how many retries
      # so, we just chalk it off as "thumbed"
      thumbed_links.append(row['link'])
  return thumbed_links
  

def links_to_be_thumbed():
  return [
    url for url in
    set(links_in_alfdb()) - set(thumbed_links_in_thumbsdb())
  ]


def alternate_domains(link):
  return link\
    .replace('//twitter.com', '//nitter.net')\


def get_youtube_info(link):
  '''
  youtube takes way too much time to load,
  and even after it does load, the screenshot is usually wrong
  because instead of the actual video, the screenshot is taken
  while an advertisement is playing.

  as an exception, for youtube, we manually download the webpage
  and scrape it for both the thumbnail url title.

  These are the lines to look out for:
  <meta property="og:title" content="title">
  <meta property="og:image" content="imgurl">
  '''
  yt_lines = subprocess.check_output(['curl', '--silent', link]).splitlines()
  title_prefix = '<meta property="og:title" content="'
  image_prefix = '<meta property="og:image" content="'
  title = ''
  image_filepath = ''
  for line in yt_lines:
    line = line.strip()
    if line.startswith(title_prefix):
      title = line[len(title_prefix):-2]
    elif line.startswith(image_prefix):
      path = line[len(image_prefix):-2]
      image_filepath = THUMBS_DIR + '/' + str(uuid4()) + '.jpg'
      subprocess.check_call([
        'curl', '--silent', path, '-o', image_filepath
      ])
  print(title, image_filepath)
  return {
    'screenshot': {
      'title': title,
      'path': image_filepath
    }
  }


def get_info(link):
  '''
  {
    'error'?: <error msg>,
    'screenshot'?: {
      'title'?: <title>,
      'path': <path>
    }
  }
  '''
  if (link.startswith('https://www.youtube.com') or
      link.startswith('http://www.youtube.com')):
    return get_youtube_info(link)

  cleaned_link = link.split()[0].strip()
  return json.loads(subprocess.check_output([
    SCREENSHOTTER,
    '--width', '800',
    '--height', '1024',
    '--base-dir', THUMBS_DIR,
    alternate_domains(cleaned_link)
  ]))


def mk_html(title, image_filename, error_text):
  html_content = ''
  if image_filename:
    html_content += u'<img src="' + image_filename + u'">'
  if title:
    html_content += u'<div class="caption">' + title + u'</div>'
  if error_text:
    html_content += u'<div class="caption">' + error_text + u'</div>'

  return u'''
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="style.css">
      </head>
      <body>
        <div id="container">''' + html_content + '''</div>
      </body>
    </html>'''


def mk_html_file(link, title=None, image_filename=None, error_text=None):
  filename = hash(link) + '.html'
  filepath = THUMBS_DIR + '/' + filename
  # if not os.path.exists(filepath):
  html = mk_html(title, image_filename, error_text)
  with open(filepath, 'w') as html_file:
    html_file.write(html.encode('utf-8'))

  return filepath


def save_thumb(link):
  info = get_info(link)
  thumbsdb = sqlite3.connect(DB_PATH)
  if 'screenshot' in info:
    screenshot = info['screenshot']
    title = screenshot.get('title', '...')
    image_filepath = screenshot['path']
    if image_filepath.startswith('http'):
      image_filename = image_filepath
    else:
      image_filename = image_filepath.split('/')[-1]
    thumbnail_filepath = mk_html_file(
      link,
      title=title,
      image_filename=image_filename
    )
    thumbsdb.execute('''
      INSERT OR REPLACE INTO linkthumbs
      (link, title, error, thumbnail_filepath)
      VALUES
      (?, ?, 0, ?)''',
      (link, title, thumbnail_filepath)
    )
  else:
    print('Failed to get thumb data for: %s: %s' % (link, info))
    error_text = info.get('error', '<something went wrong>')
    thumbnail_filepath = mk_html_file(link, error_text=error_text)
    thumbsdb.execute('''
      INSERT OR REPLACE INTO linkthumbs
      (link, title, error, error_text, thumbnail_filepath)
      VALUES
      (?, "...", 1, ?, ?)''',
      (link, error_text, thumbnail_filepath)
    )
  thumbsdb.commit()
  

thumbs = {}
# for link in tqdm(links_to_be_thumbed()):
for link in links_to_be_thumbed():
  save_thumb(link)
